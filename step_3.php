
<?php
session_start();
$uname = $_SESSION["uname"];
$dbname = $_SESSION["dbname"];
$dbpassword = $_SESSION["dbpassword"];
$domain = $_SESSION["domain"];

if(isset($_POST['submit'])){
    $servername = "localhost";
    $username = $uname;
    $password = $dbpassword;
    $dbname = $dbname;
    
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
    }
    
    $sql = "INSERT INTO lmsdb (dbname, username, password)
    VALUES ('$dbname', '$username', '$password')";
    
    if ($conn->query($sql) === TRUE) {
        header("refresh:2;url=$domain");
    } else {
      echo "Error: " . $sql . "<br>" . $conn->error;
    }
    
    $conn->close();
   
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>

<div class="container" style="width:700px;">  
   <h3 align="center">3 Step</h3>  
   <br/>
   <form action="/step_3.php" method="POST">
   
      
      <input type="hidden" class="form-control" id="dbname" value="<?php echo $uname;?>" name="dbname" >
      <input type="hidden" class="form-control" id="uname" value="<?php echo $dbname;?>" name="uname" >
      <input type="hidden" class="form-control" id="dbpassword" value="<?php echo $dbpassword;?>"  name="dbpassword">
    <button type="submit" class="btn btn-primary">Finish</button>
  </form>
  </div> 
</body>
</html>
