<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>

<div class="container" style="width:700px;">  
   <h3 align="center">1 Step</h3>  
   <br />
   <form action="/step_2.php" method="POST">
   <div class="form-group">
      <label for="domain">Domain :</label>
      <input type="uname" class="form-control" id="domain" placeholder="Enter Domain" name="domain" required>
    </div>  
    <div class="form-group">
      <label for="dbname">Database name:</label>
      <input type="dbname" class="form-control" id="dbname" placeholder="Enter Database name" name="dbname" required>
    </div>
    <div class="form-group">
      <label for="uname">Username :</label>
      <input type="uname" class="form-control" id="uname" placeholder="Enter Database User name" name="uname" required>
    </div>
    <div class="form-group">
      <label for="dbpassword">Password:</label>
      <input type="password" class="form-control" id="dbpassword" placeholder="Enter Database password" name="dbpassword">
    </div>
    
    <div class="form-group form-check">
      <label class="form-check-label">
        <input class="form-check-input" type="checkbox" name="remember"> Remember me
      </label>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
  </div> 
</body>
</html>
